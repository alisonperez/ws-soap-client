/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adp.soapclient.converters;

import com.w3schools.webservices.TempConvert;
import com.w3schools.webservices.TempConvertSoap;

/**
 *
 * @author Mairelin Roque
 */
public class TemperatureConverter {
    private final TempConvert tempConvert;
    private final TempConvertSoap soap;
    
    public TemperatureConverter(){
        tempConvert = new TempConvert();
        soap = tempConvert.getTempConvertSoap();
    }
    
    public String convertCelsiusToFahrenheit(String degrees){
        return soap.celsiusToFahrenheit(degrees);
    }
    
    public String convertFahrenheitToCelsius(String degrees){
        return soap.fahrenheitToCelsius(degrees);
    }
}
