/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adp.soapclient;

import com.adp.soapclient.converters.TemperatureConverter;

/**
 *
 * @author Mairelin Roque
 */
public class Main {
    public static void main(String[] args) {
        TemperatureConverter converter = new TemperatureConverter();
        
        System.out.println("Celsius to Fahrenheit: " + converter.convertCelsiusToFahrenheit("25"));
        
        System.out.println("Fahrenheit to Celsius: " + converter.convertFahrenheitToCelsius("77"));
        
    }
}
